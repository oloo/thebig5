-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2014 at 04:41 PM
-- Server version: 5.6.12
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thebig5`
--
CREATE DATABASE IF NOT EXISTS `thebig5` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `thebig5`;

-- --------------------------------------------------------

--
-- Table structure for table `bet`
--

DROP TABLE IF EXISTS `bet`;
CREATE TABLE IF NOT EXISTS `bet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `systemuser_id` int(11) NOT NULL,
  `datetimecreated` datetime NOT NULL,
  `openingtime` datetime NOT NULL,
  `closingtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sfdvkhsdfvdfklhvsddff` (`systemuser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bet`
--

INSERT INTO `bet` (`id`, `systemuser_id`, `datetimecreated`, `openingtime`, `closingtime`) VALUES
(1, 1, '2014-03-02 00:00:00', '2014-03-03 00:00:00', '2014-03-07 00:00:00'),
(2, 1, '2014-03-11 00:00:00', '2014-03-11 00:00:00', '2014-03-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `betoption`
--

DROP TABLE IF EXISTS `betoption`;
CREATE TABLE IF NOT EXISTS `betoption` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bet_id` bigint(20) NOT NULL,
  `competition_id` int(11) NOT NULL,
  `datetimecreated` datetime NOT NULL,
  `firstteam` int(11) NOT NULL,
  `secondteam` int(11) NOT NULL,
  `firstteamscore` int(11) DEFAULT NULL,
  `secondteamscore` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eesfgoifvgkfjjffj` (`bet_id`),
  KEY `kefvmndfsmndsfndsf` (`competition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `betoption`
--

INSERT INTO `betoption` (`id`, `bet_id`, `competition_id`, `datetimecreated`, `firstteam`, `secondteam`, `firstteamscore`, `secondteamscore`) VALUES
(1, 1, 1, '2014-03-03 00:00:00', 1, 2, 1, 1),
(2, 1, 2, '2014-03-04 00:00:00', 3, 4, 2, 1),
(3, 1, 5, '2014-03-03 00:00:00', 5, 6, 3, 1),
(4, 1, 6, '2014-03-04 00:00:00', 7, 8, 2, 2),
(5, 2, 5, '2014-03-12 00:00:00', 5, 6, NULL, NULL),
(6, 2, 6, '2014-03-12 00:00:00', 7, 8, NULL, NULL),
(7, 2, 1, '2014-03-12 00:00:00', 2, 3, NULL, NULL),
(8, 2, 1, '2014-03-12 00:00:00', 6, 7, NULL, NULL),
(9, 2, 2, '2014-03-12 00:00:00', 8, 9, NULL, NULL),
(10, 1, 2, '2014-03-03 00:00:00', 3, 8, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
CREATE TABLE IF NOT EXISTS `competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `abbreviation` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `competition`
--

INSERT INTO `competition` (`id`, `name`, `abbreviation`) VALUES
(1, 'Champions League', 'CL'),
(2, 'Premier League', 'PL'),
(5, 'World Cup', 'WC'),
(6, 'European Cup', 'EC');

-- --------------------------------------------------------

--
-- Table structure for table `systemuser`
--

DROP TABLE IF EXISTS `systemuser`;
CREATE TABLE IF NOT EXISTS `systemuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `othername` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `datecreated` date NOT NULL,
  `lastmodified` date DEFAULT NULL,
  `lastlogindate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `systemuser`
--

INSERT INTO `systemuser` (`id`, `firstname`, `othername`, `username`, `password`, `datecreated`, `lastmodified`, `lastlogindate`) VALUES
(1, 'James', 'Oloo', 'oloo', 'james?20', '2014-03-11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `abbreviation` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `abbreviation`) VALUES
(1, 'Arsenal FC', 'ARS'),
(2, 'Manchester United', 'MANU'),
(3, 'Arsenal FC', 'ARS'),
(4, 'Manchester United', 'MANU'),
(5, 'Liverpool FC', 'LIV'),
(6, 'Brazil', 'BRA'),
(7, 'France', 'FRA'),
(8, 'Bayern Munich', 'BAY'),
(9, 'Real Madrid', 'RMDRD');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) DEFAULT NULL,
  `othername` varchar(100) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` int(11) DEFAULT NULL,
  `datecreated` date NOT NULL,
  `lastmodified` date DEFAULT NULL,
  `lastlogindate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `othername`, `username`, `password`, `datecreated`, `lastmodified`, `lastlogindate`) VALUES
(1, 'John', 'Muwanguzi', '0779500795', 12345, '2014-03-12', NULL, NULL),
(2, NULL, NULL, '0794240433', NULL, '2014-03-12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userbet`
--

DROP TABLE IF EXISTS `userbet`;
CREATE TABLE IF NOT EXISTS `userbet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bet_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `datetimecreated` datetime NOT NULL,
  `paid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `efkjfhfkdhfdhssdfkj` (`bet_id`),
  KEY `kjefyjgaaeqppo` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `userbet`
--

INSERT INTO `userbet` (`id`, `bet_id`, `user_id`, `datetimecreated`, `paid`) VALUES
(1, 1, 1, '2014-03-05 00:00:00', 1),
(2, 2, 1, '2014-03-13 00:00:00', 1),
(3, 1, 2, '2014-03-05 00:00:00', 1),
(4, 2, 2, '2014-03-13 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userbetoption`
--

DROP TABLE IF EXISTS `userbetoption`;
CREATE TABLE IF NOT EXISTS `userbetoption` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userbet_id` bigint(20) NOT NULL,
  `betoption_id` bigint(20) NOT NULL,
  `firstteamprediction` int(11) NOT NULL,
  `secondteamprediction` int(11) NOT NULL,
  `datetimecreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `efopefiudshgsdndsn` (`userbet_id`),
  KEY `dsfvfdsjdsndsndsn` (`betoption_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `userbetoption`
--

INSERT INTO `userbetoption` (`id`, `userbet_id`, `betoption_id`, `firstteamprediction`, `secondteamprediction`, `datetimecreated`) VALUES
(1, 1, 1, 2, 0, '2014-03-11 00:00:00'),
(2, 1, 2, 3, 1, '2014-03-10 00:00:00'),
(3, 1, 3, 2, 4, '2014-03-11 00:00:00'),
(4, 1, 4, 3, 3, '2014-03-10 00:00:00'),
(5, 1, 10, 0, 1, '2014-03-10 00:00:00'),
(6, 2, 5, 4, 4, '2014-03-11 00:00:00'),
(7, 2, 6, 3, 1, '2014-03-10 00:00:00'),
(8, 2, 7, 4, 1, '2014-03-10 00:00:00'),
(9, 2, 8, 2, 1, '2014-03-10 00:00:00'),
(10, 2, 9, 1, 1, '2014-03-10 00:00:00'),
(11, 3, 1, 0, 2, '2014-03-11 00:00:00'),
(12, 3, 2, 1, 3, '2014-03-10 00:00:00'),
(13, 3, 3, 1, 2, '2014-03-11 00:00:00'),
(14, 3, 4, 2, 0, '2014-03-10 00:00:00'),
(15, 3, 10, 5, 11, '2014-03-10 00:00:00'),
(16, 4, 5, 1, 4, '2014-03-11 00:00:00'),
(17, 4, 6, 4, 2, '2014-03-10 00:00:00'),
(18, 4, 7, 5, 3, '2014-03-10 00:00:00'),
(19, 4, 8, 1, 3, '2014-03-10 00:00:00'),
(20, 4, 9, 3, 0, '2014-03-10 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bet`
--
ALTER TABLE `bet`
  ADD CONSTRAINT `aefvfeeefeeferwef` FOREIGN KEY (`systemuser_id`) REFERENCES `systemuser` (`id`);

--
-- Constraints for table `betoption`
--
ALTER TABLE `betoption`
  ADD CONSTRAINT `dsvdfvfdfdfgfsgd` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`id`),
  ADD CONSTRAINT `sdsdfdfgdfgdf` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`id`);

--
-- Constraints for table `userbet`
--
ALTER TABLE `userbet`
  ADD CONSTRAINT `kfeuweeeepo` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `liygfsdjsdmsddsf` FOREIGN KEY (`bet_id`) REFERENCES `bet` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
