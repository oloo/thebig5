(ns big5.repos.bet-repo-test
  (:use midje.sweet)
  (:use korma.core)
  (:use korma.db)
  (:require [clojure.string :as str]
            [big5.repos.bet-repo :as bet-repo]
            [big5.util :as util]
            [big5.configurations :as config]))

(with-state-changes [(before 
                       :facts (defdb prod (mysql {:db "thebig5test"
                                                  :user "root"
                                                  :password "WPKDx5123^"
                                                  ;; optional keys
                                                  :host "localhost"
                                                  :port "3306"})))
                     (around :facts (transaction ?form (rollback)))]  
  (facts 
    "Can test bet repo"
  
  ; (bet-repo/add-bet 1 (util/get-sql-datetime) (util/get-sql-datetime))
   ;  => (just {:GENERATED_KEY  number?})
  
    (bet-repo/get-all-bets)
    => []))