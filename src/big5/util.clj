(ns big5.util
  (:import java.util.Date)
  (:import java.text.SimpleDateFormat)
  (:require [clj-json.core :as json]
            [clj-time.core :as clj-t]
            [taoensso.timbre :as timbre]))

(defn format-time
  "formats the time using SimpleDateFormat, the default format is
   \"dd MMM, yyyy\" and a custom one can be passed in as the second argument"
  ([time] (format-time time "dd MMM, yyyy"))
  ([time fmt]
    (.format (new java.text.SimpleDateFormat fmt) time)))

(defn json-response 
  "Return a Json encoded response for the data
   TODO: Move this to a generalized namespace of utilities"
  [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/json"}
   :body (json/generate-string data)})

(defn pdf-response
  "Return Pdf encoded response for the data"
  [name]
  {:status 200
   :headers {"Content-Type" "application/pdf"}
   :body (clojure.java.io/input-stream name)})

(defn get-sql-date
  []
  (let [today (Date.)
        formatter (SimpleDateFormat. "yyyy-MM-dd")]
    (.format formatter today)))

(defn get-sql-datetime
  []
  (let [today (Date.)
        formatter (SimpleDateFormat. "yyyy-MM-dd hh:mm:ss")]
    (.format formatter today)))

(defn find-first
         [f coll]
         (first (filter f coll)))