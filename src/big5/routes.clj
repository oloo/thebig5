(ns big5.routes
  (:use compojure.core)
  (:require [big5.util :as util]
            [big5.repos.bet-repo :as bet-repo]
            [big5.repos.user-bet-repo :as user-bet-repo]
            [big5.repos.team-repo :as team-repo]
            [big5.repos.competition-repo :as competition-repo]))

(defn get-bets
  []
  (bet-repo/get-all-bets-with-options-formatted))

(defn get-user-bets
  []
  (user-bet-repo/get-all-user-bets-with-options))

(defn get-teams
  []
  (team-repo/get-all-teams))

(defn get-competitions
  []
  (competition-repo/get-all-competitions))

(defroutes app-routes
  (GET "/bet/all" [] (util/json-response (get-bets)))
  (GET "/userbet/all" [] (util/json-response (get-user-bets)))
  (GET "/team/all" [] (util/json-response (get-teams)))
  (GET "/competition/all" [] (util/json-response (get-competitions))))

