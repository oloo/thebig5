(ns big5.models
  (:use korma.core)
  (:use korma.db)
  (:require [clojure.string :as str]
            [big5.configurations :as config]))

(defdb prod (mysql (:db-connection config/config-map)))

(declare user systemuser bet team betoption competition
         userbet userbetoption)
 
(defentity user
  (has-many userbet))

(defentity systemuser)

(defentity userbet
  (has-many userbetoption)
  (belongs-to user)
  (belongs-to bet))

(defentity bet
  (has-many betoption))

(defentity betoption
  (belongs-to competition)
  (belongs-to bet))

(defentity team)

(defentity userbetoption
  (belongs-to betoption)
  (belongs-to userbet))

(defentity competition
  (has-many betoption))