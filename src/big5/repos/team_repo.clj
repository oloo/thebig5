(ns big5.repos.team-repo
  (:use korma.db)
  (:use korma.core)
  (:use big5.models)
  (:require [clojure.string :as str]
            [big5.util :as util]))

(defn get-all-teams
  []
  (select team
          (fields :id :name :abbreviation)))

(defn get-team-by-id
  [id]
    (first (select team
             (where {:id id})
             (fields :id :name :abbreviation))))