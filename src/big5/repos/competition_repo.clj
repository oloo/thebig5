(ns big5.repos.competition-repo
  (:use korma.db)
  (:use korma.core)
  (:use big5.models)
  (:require [clojure.string :as str]
            [big5.util :as util]))

(defn get-all-competitions
  []
  (select competition
          (fields :id :name :abbreviation)))

(defn get-competition-by-id
  [id]
  (first (select competition 
          (where {:id id})
          (fields :id :name :abbreviation))))