(ns big5.repos.bet-repo
  (:use korma.db)
  (:use korma.core)
  (:use big5.models)
  (:require [clojure.string :as str]
            [big5.util :as util]
            [big5.repos.team-repo :as team-repo]))

(defn get-all-bets
  []
  (select bet      
      (fields :id  :systemuser_id)))

(defn get-all-bets-with-options
  []
  (select betoption
          (with bet)
          (join competition (= :competition.id :competition_id))
          (fields :id [:competition.name :comp] :firstteam :secondteam
                  :firstteamscore :secondteamscore [:bet_id :bet-id] 
                  [:bet.openingtime :openingtime]
                  [:bet.closingtime :closingtime])))

(defn get-all-bets-with-options-formatted
  []
  (let [all-bets (get-all-bets-with-options)
        teams (team-repo/get-all-teams)]
    (into []
          (for [{id :id comp :comp firstteam :firstteam secondteam :secondteam
                 firstteamscore :firstteamscore secondteamscore :secondteamscore
                 bet-id :bet-id openingtime :openingtime closingtime :closingtime} all-bets]
            
            {:id id :comp comp :first-team (:name (util/find-first #(= firstteam (:id %)) teams))
            :second-team (:name (util/find-first #(= secondteam (:id %)) teams))
            :first-team-score firstteamscore :second-team-score secondteamscore
            :opening-time (.toString openingtime) :closing-time (.toString closingtime)}))))

(defn add-bet
  [system-user-id opening-time closing-time]
  (insert bet
          (values {:systemuser_id system-user-id :openingtime opening-time 
                   :datetimecreated (util/get-sql-datetime) :closingtime closing-time})))

(defn add-bet-options
  [bet-id competition-id first-team second-team]
  (insert betoption
          (values {:bet_id bet-id :competition_id competition-id
                   :firstteam first-team :secondteam second-team
                   :datetimecreated (util/get-sql-datetime)})))

(defn add-bet-with-options
  [system-user-id opening-time closing-time & bet-options]
  (transaction
  (try
    (let [bet-id (:GENERATED_KEY (add-bet system-user-id opening-time closing-time))]
      (map (fn [{competition-id :competition-id first-team :first-team second-team :second-team}]
             (add-bet-options bet-id competition-id first-team second-team))
           bet-options))
  (catch Exception e
    (do
      (rollback)
      (taoensso.timbre/info (str " Error adding bet. Details: " 
                                 (.getMessage e))))))))