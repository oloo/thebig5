(ns big5.repos.user-bet-repo
  (:use korma.db)
  (:use korma.core)
  (:use big5.models)
  (:require [clojure.string :as str]
            [big5.util :as util]))

(defn get-all-user-bets
  []
  (select userbet
          (fields :id :bet_id :user_id :paid)))

(defn get-all-user-bets-with-options
  []
  (select userbetoption
          (with userbet)
          (join user (= :userbet.user_id :user.id))
          (fields :id :firstteamprediction :secondteamprediction 
                  :user.username)))