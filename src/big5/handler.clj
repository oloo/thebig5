(ns big5.handler
  (:use big5.routes
        compojure.core)
  (:require 
            [noir.util.middleware :as middleware]
            [compojure.route :as route]
            [taoensso.timbre :as timbre]
            [com.postspectacular.rotor :as rotor]))

(defroutes other-routes
  (route/resources "/")
  (route/not-found "Page Not Found"))

(defn init
  []
  (timbre/set-config!
    [:appenders :rotor]
    {:min-level :info
     :enabled? true
     :async? false ; should be always false for rotor
     :max-message-per-msecs nil
     :fn rotor/append})
      
  (timbre/set-config!
    [:shared-appender-config :rotor]
    {:path "{{sanitized}}.log" :max-size 10000 :backlog 10})
   
  (timbre/info "Big 5 has been fired up!..."))

(defn destroy
  []
  (println "shutting down..."))

;;append your application routes to the all-routes vector
(def all-routes [app-routes other-routes])

(def app (-> all-routes
             middleware/app-handler            
             ring.middleware.keyword-params/wrap-keyword-params
             ring.middleware.nested-params/wrap-nested-params
             ring.middleware.params/wrap-params
             (ring.middleware.session/wrap-session)
             ))

(def war-handler (middleware/war-handler app))
